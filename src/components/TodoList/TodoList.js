import React from 'react'
import TodoItem from "../TodoItem/TodoItem"

function TodoList(props) {
  return (
    <section className="main">
      <ul className="todo-list">
        {props.todos.map((todo) => (
          <TodoItem
            handleDelete={props.handleDelete}
            handleToggle={props.handleToggle}
            id={todo.id}
            key={todo.id}
            title={todo.title}
            completed={todo.completed}
          />
        ))}
      </ul>
    </section>
  )
}
export default TodoList