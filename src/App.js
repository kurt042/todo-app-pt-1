import React, { useState } from "react";
import todosList from "./todos.json"
import TodoList from "./components/TodoList/TodoList";
import { v4 } from "uuid";
function App() {
  const [todos, setTodos] = useState(todosList)

  const newItem = () => {
    let newId = v4()
    let content = document.getElementById("theInput").value
    if (content.length < 2) { return }
    let newTodo = {
      title: content,
      id: newId,
      userId: 1,
      completed: false
    }
    let tempArray = [...todos]
    tempArray.push(newTodo)
    document.getElementById("theInput").value = ""
    setTodos(tempArray)
  }

  const handleKeyPress = event => {
    if (event.key === 'Enter') {
      event.preventDefault()
      newItem()
    } else return
  }

  const handleToggle = (id) => {
    const newTodos = todos.map(item => {
      if (item.id === id) {
        item.completed = !item.completed
      }
      return item
    })
    setTodos(newTodos)
  }

  const handleDelete = (id) => {
    const newTodos = todos.filter(item => (item.id !== id))
    setTodos(newTodos)
  }

  const handleDeleteCompleted = () => {
    const newTodos = todos.filter(item => (item.completed !== true))
    setTodos(newTodos)
  }

  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <input onKeyPress={handleKeyPress} id="theInput" className="new-todo" placeholder="What needs to be done?" autoFocus />
      </header>
      <TodoList
        handleDelete={handleDelete}
        handleToggle={handleToggle}
        todos={todos}
      />
      <footer className="footer">
        <span className="todo-count">
          <strong>{
            todos.filter(item => item.completed !== true).length
          }</strong> item(s) left
          </span>
        <button
          className="clear-completed"
          onClick={handleDeleteCompleted}>Clear completed
        </button>
      </footer>
    </section>
  );
}

export default App;